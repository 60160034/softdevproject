/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinal;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import projectfinalcode.Products;
import projectfinalcode.ProductsDAO;

/**
 *
 * @author Mac
 */
public class ProductAddProductPanel extends javax.swing.JPanel {

    public static String name;
    public static String quantity;
    public static String price;
    public static String type;
    /**
     * Creates new form Addproduct
     */
    public ProductAddProductPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        productname = new javax.swing.JLabel();
        productquantity = new javax.swing.JLabel();
        productprice = new javax.swing.JLabel();
        producttype = new javax.swing.JLabel();
        add = new javax.swing.JButton();
        back = new javax.swing.JButton();
        inputproname = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<String>();
        inputproquantity = new javax.swing.JTextField();
        inputproprice = new javax.swing.JTextField();
        productunit = new javax.swing.JLabel();
        productname1 = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(780, 450));

        productname.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        productname.setText("ชื่อสินค้า :");

        productquantity.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        productquantity.setText("จำนวน :");

        productprice.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        productprice.setText("ราคา :");

        producttype.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        producttype.setText("ประเภท :");

        add.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        add.setText("เพิ่ม");
        add.setMaximumSize(new java.awt.Dimension(150, 80));
        add.setMinimumSize(new java.awt.Dimension(150, 80));
        add.setPreferredSize(new java.awt.Dimension(150, 80));
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        back.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        back.setText("ย้อนกลับ");
        back.setMaximumSize(new java.awt.Dimension(150, 80));
        back.setMinimumSize(new java.awt.Dimension(150, 80));
        back.setPreferredSize(new java.awt.Dimension(150, 80));
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        inputproname.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        inputproname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputpronameActionPerformed(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "สินค้า", "บริการ" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        inputproquantity.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        inputproquantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputproquantityActionPerformed(evt);
            }
        });

        inputproprice.setFont(new java.awt.Font("Tahoma", 0, 22)); // NOI18N
        inputproprice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputpropriceActionPerformed(evt);
            }
        });

        productunit.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        productunit.setText("หน่วย");

        productname1.setFont(new java.awt.Font("Angsana New", 1, 48)); // NOI18N
        productname1.setText("เพิ่มสินค้า");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(productquantity)
                                        .addGap(53, 53, 53))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(producttype)
                                            .addComponent(productprice))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(inputproprice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(productname)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(inputproquantity, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(productunit))
                                    .addComponent(inputproname, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                        .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(152, 152, 152))))
            .addGroup(layout.createSequentialGroup()
                .addGap(326, 326, 326)
                .addComponent(productname1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(productname1)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(productname)
                    .addComponent(inputproname, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(productquantity)
                    .addComponent(inputproquantity, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(productunit))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(productprice)
                    .addComponent(inputproprice, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(producttype, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jComboBox1)))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        AllJFrame mainFrame = (AllJFrame) SwingUtilities.getWindowAncestor(this);
        mainFrame.changeToProductPanel();
    }//GEN-LAST:event_backActionPerformed

    private void inputpronameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputpronameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputpronameActionPerformed

    private void inputproquantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputproquantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputproquantityActionPerformed

    private void inputpropriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputpropriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputpropriceActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
    
         int inputaddproduct=JOptionPane.showConfirmDialog(this, "คุณต้องการที่จะเพิ่มสินค้าหรือบริการหรือไม่ ?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
  if(inputaddproduct==JOptionPane.YES_OPTION){
        getValue();
        int quantityInt = Integer.parseInt(quantity);
        int priceInt = Integer.parseInt(price);
        Products product = new Products(-1,name,type,quantityInt,priceInt);
        ProductsDAO.insert(product);
            AllJFrame mainFrame = (AllJFrame) SwingUtilities.getWindowAncestor(this);
        mainFrame.changeToProductPanel();
  }else{
          // mainFrame.changeToDepositPetPanel();
  }
       
     //  AllJFrame mainFrame = (AllJFrame) SwingUtilities.getWindowAncestor(this);
        //mainFrame.changeToProductConfirmAddPanel(); 
    }//GEN-LAST:event_addActionPerformed

    public void getValue(){
        name = inputproname.getText();
        quantity = inputproquantity.getText();
        price = inputproprice.getText();
        type = (String) jComboBox1.getSelectedItem();
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JButton back;
    private javax.swing.JTextField inputproname;
    private javax.swing.JTextField inputproprice;
    private javax.swing.JTextField inputproquantity;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel productname;
    private javax.swing.JLabel productname1;
    private javax.swing.JLabel productprice;
    private javax.swing.JLabel productquantity;
    private javax.swing.JLabel producttype;
    private javax.swing.JLabel productunit;
    // End of variables declaration//GEN-END:variables
}
