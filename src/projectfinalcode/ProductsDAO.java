/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import projectfinalcode.database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author informatics
 */
public class ProductsDAO {
    public static boolean insert(Products pro){
       Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
           String sql = "INSERT INTO Products (\n" +
"                         name,\n" +
"                         type,\n" +
"                         quantity,\n" +
"                         price\n" +
"                     )\n" +
"                     VALUES (\n" +
"                     '%s',\n" +
"                         '%s',\n" +
"                         %d,\n" +
"                         %d\n" +
"                           );";
        System.out.println(String.format(sql,pro.getName(),pro.getType(),pro.getQuantity(),pro.getPrice()));
        stm.execute(String.format(sql,pro.getName(),pro.getType(),pro.getQuantity(),pro.getPrice()));
        database.Closedatabase();
        return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return true;
    }
    
    public static boolean update(Products pro){
        String sql = "UPDATE Products\n" +
"   SET name = '%s',\n" +
"       type = '%s',\n" +
"       quantity = %d,\n" +
"       price = %d\n" +
" WHERE Id = %d \n" +"";
        Connection conn = database.Connectdatabase();
        Statement stm ;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, pro.getName(),pro.getType(),pro.getQuantity(),pro.getPrice(),pro.getId()));
            database.Closedatabase();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return true;
    }
    
    public static boolean delete(Products pro){
        String sql = "DELETE FROM Products\n" +
"      WHERE Id = %d";
        Connection conn = database.Connectdatabase();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,pro.getId()));
        database.Closedatabase();
        return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();    
        return true;
    }
    
    public static ArrayList<Products> getProducts(){
        ArrayList<Products> list = new ArrayList();
        database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n" +
"       name,\n" +
"       type,\n" +
"       quantity,\n" +
"       price\n" +
"  FROM Products";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                Products pro = rsToObj(rs);
                list.add(pro);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }
    
    private static Products rsToObj(ResultSet rs) throws SQLException {
        Products pro;
        pro = new Products();
        pro.setId(rs.getInt("id"));
        pro.setName(rs.getString("name"));
        pro.setType(rs.getString("type"));
        pro.setQuantity(rs.getInt("quantity"));
        pro.setPrice(rs.getInt("price"));
        return pro;
    }
    
    public static Products getProduct(int proId){
        String sql = "SELECT * FROM Products WHERE Id= %d ";
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql,proId));
            if(rs.next()){
                Products pro = rsToObj(rs);
                database.Closedatabase(); 
                return pro;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase(); 
        return null;
    }
}
