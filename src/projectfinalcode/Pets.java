/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

/**
 *
 * @author informatics
 */
public class Pets {
    int Id;
    String name;
    int age;
    double weight;
    String specie;
    String type;
    String detail;
    int id_customer;


    public int getId_customer() {
        return id_customer;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Pets{" + "Id=" + Id + ", name=" + name + ", age=" + age + ", weight=" + weight + ", specie=" + specie + ", type=" + type + ", detail=" + detail + ", id_customer=" + id_customer + '}';
    }

    public Pets(int Id, String name, int age, double weight, String specie, String type, String detail, int id_customer) {
        this.Id = Id;
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.specie = specie;
        this.type = type;
        this.detail = detail;
        this.id_customer = id_customer;
    }
    
    public Pets() {
        this.Id = -1;
    }
    
}
