/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import projectfinalcode.database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author informatics
 */
public class RoomsDAO {
        public static boolean update(Rooms room){
        String sql = "UPDATE Rooms\n" +
"   SET name = '%s',\n" +
"       status = '%d'\n" +
" WHERE Id = %d ";
        Connection conn = database.Connectdatabase();
        Statement stm ;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, room.getName(),room.getStatus(),room.getId()));
            database.Closedatabase();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(RoomsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return true;
    }
        
    public static ArrayList<Rooms> getRooms(){
        ArrayList<Rooms> list = new ArrayList();
        Connection conn = database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n" +
"       name,\n" +
"       status\n" +
"  FROM Rooms";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                Rooms room = rsToObj(rs);
                list.add(room);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }
    
    private static Rooms rsToObj(ResultSet rs) throws SQLException {
        Rooms room;
        room = new Rooms();
        room.setId(rs.getInt("id"));
        room.setName(rs.getString("name"));
        room.setStatus(rs.getInt("status"));
        return room;
    }
    
    public static Rooms getRoom(int roomId){
        String sql = "SELECT * FROM Rooms WHERE Id= %d ";
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql,roomId));
            if(rs.next()){
                Rooms room = rsToObj(rs);
                database.Closedatabase(); 
                return room;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase(); 
        return null;
    }
    
      
    

    
    
    
}

