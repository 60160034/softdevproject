/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Oatmeal
 */
public class ProductTableModel extends AbstractTableModel {
    String[] columnNames = {"id","name","quantity","type","price"};
    ArrayList<Products> productList = ProductsDAO.getProducts();
    
    public ProductTableModel(){
    
    }
    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }

    
    
    @Override
    public int getRowCount() {
        return productList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Products product = productList.get(rowIndex);
        if(product == null) return "";
            switch(columnIndex){
                case 0: return product.getId();
                case 1: return product.getName();
                case 2: return product.getQuantity();
                case 3: return product.getType();
                case 4: return product.getPrice();
            }
            return "";
    }
    
}
