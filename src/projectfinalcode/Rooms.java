/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

/**
 *
 * @author informatics
 */
public class Rooms {
    int Id ;
    String name;
    int status;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Rooms{" + "Id=" + Id + ", name=" + name + ", status=" + status + '}';
    }

    public Rooms(int Id, String name, int status) {
        this.Id = Id;
        this.name = name;
        this.status = status;
    }
    
     public Rooms(){
         this.Id = -1;
     }
}
