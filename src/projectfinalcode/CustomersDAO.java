/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectfinalcode;

import projectfinalcode.database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static projectfinalcode.database.conn;

/**
 *
 * @author informatics
 */
public class CustomersDAO {
    
    public static boolean insert(Customer cus){
       Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
           String sql = "INSERT INTO Customers (\n" +
"                       name,\n" +
"                       surname,\n" +
"                       tel\n" +
"                      )\n" +
"                      VALUES (\n" +
"                         '%s', \n" +
"                         '%s', \n" +
"                         '%s'\n" +
"                      );";
        System.out.println(String.format(sql,cus.getName(),cus.getSurname(),cus.getTel()));
        stm.execute(String.format(sql,cus.getName(),cus.getSurname(),cus.getTel()));
        database.Closedatabase();
        return true;
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       database.Closedatabase();
        return true;
    }
    
    public static boolean update(Customer customer){
        String sql = "UPDATE Customers\n" +
"   SET name = '%s',\n" +
"       surname = '%s',\n" +
"       tel = '%s'\n" +
" WHERE Id = %d ";
        Connection conn = database.Connectdatabase();
        Statement stm ;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, customer.getName(),customer.getSurname(),customer.getTel(),customer.getId()));
            database.Closedatabase();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return true;
    }
    
    public static boolean delete(Customer customer){
        String sql = "DELETE FROM Customers\n" +
"      WHERE Id = %d";
        Connection conn = database.Connectdatabase();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,customer.getId()));
        database.Closedatabase();
        return ret;
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();    
        return true;
    }
    
    public static ArrayList<Customer> getCustomers(){
        ArrayList<Customer> list = new ArrayList();
        database.Connectdatabase();
        try {
            String sql = "SELECT Id,\n" +
"       name,\n" +
"       surname,\n" +
"       tel\n" +
"  FROM Customers";
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                Customer cus = rsToObj(rs);
                list.add(cus);
            }
            database.Closedatabase();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase();
        return null;
    }
    
    private static Customer rsToObj(ResultSet rs) throws SQLException {
        Customer cus;
        cus = new Customer();
        cus.setId(rs.getInt("id"));
        cus.setName(rs.getString("name"));
        cus.setSurname(rs.getString("surname"));
        cus.setTel(rs.getString("tel"));
        return cus;
    }
    
    public static Customer getCustomer(int cusId){
        String sql = "SELECT * FROM Customers WHERE Id= %d ";
        Connection conn = database.Connectdatabase();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql,cusId));
            if(rs.next()){
                Customer cus = rsToObj(rs);
                database.Closedatabase(); 
                return cus;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.Closedatabase(); 
        return null;
    }
    
}
